<?php if ( ! defined('WOODMART_THEME_DIR')) exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------------------------------
 * Default options array while Redux is not installed
 * ------------------------------------------------------------------------------------------------
 */

return apply_filters( 'woodmart_get_base_options', array(
	'main_layout' => 'full-width',
	'page-title-size' => 'small',
	'sidebar_width' => 3,
	'blog_sidebar_width' => 3,
	'blog_design' => 'default',
	'products_hover' => 'alt',
	'footer-layout' => 13,
	'disable_footer' => true,
	'disable_copyrights' => true,
	'copyrights-layout' => 'centered',
	'copyrights' => '<small><a href="http://woodmart.xtemos.com"><strong>WOODMART</strong></a> <i class="fa fa-copyright"></i>  2018 CREATED BY <a href="http://xtemos.com"><strong><span style="color: red; font-size: 12px;">X</span>-TEMOS STUDIO</strong></a>. PREMIUM E-COMMERCE SOLUTIONS.</small>',
	'blog_excerpt' => 'full',
	'page-title-design' => 'centered',
	'page-title-color' => 'light',
	'custom_css' => '.page-title-default { background-color: #0a0a0a; }',
	'page_comments' => true,
	'shop_view' => 'grid',
	'products_columns' => 3,
	'products_columns_mobile' => 2,
	'thums_position' => 'bottom',
	'product_short_description' => true,
	'product_tabs_layout' => 'tabs',
	'related_products' => true,
	'hover_image' => true,
	'products_hover' => 'base',
	'blog_design' => 'default',
) );